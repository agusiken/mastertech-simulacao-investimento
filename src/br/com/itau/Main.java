package br.com.itau;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        double taxa = 1.07;

        Scanner scanner = new Scanner(System.in);

        System.out.print("Valor do capital a ser investido: R$");
        double capital = scanner.nextDouble();

        System.out.print("Quantidade de meses desejado: ");
        int meses = scanner.nextInt();

        Impressora.imprimir(new Montante(meses,capital,taxa));
    }
}
