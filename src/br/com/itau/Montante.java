package br.com.itau;

public class Montante {
    private int meses;
    private double capital, taxa;

    public Montante(int meses, double capital, double taxa){
        this.meses = meses;
        this.capital = capital;
        this.taxa = taxa;
    }

    public int getMeses() {
        return meses;
    }

    public void setMeses(int meses) {
        this.meses = meses;
    }

    public double getCapital() {
        return capital;
    }

    public void setCapital(double capital) {
        this.capital = capital;
    }

    public double getTaxa() {
        return taxa;
    }

    public void setTaxa(double taxa) {
        this.taxa = taxa;
    }

    public double getSimulacao(){
        double CF = capital;

        for(int i = 0; i < meses; i++) {
            CF *= taxa;
        }
        return CF;
    }

}
